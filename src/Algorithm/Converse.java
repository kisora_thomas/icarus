package Algorithm;

import java.util.*;

import Models.Interrogative;

import java.io.*;

import Objects.Conversation;
import Objects.Delta;
import Objects.Response;
import Objects.Sentence;
import Utilities.Dialogs;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.ArrayCoreMap;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.io.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.sentiment.*;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.util.*;

public class Converse {

	public static Conversation c = new Conversation();
	public static Scanner input = new Scanner(System.in);
	public static Properties props = new Properties();
	public static PrintWriter out;
	public static Annotation annotation;
	public static String response = "";

	public static void main(String[] args) throws Throwable {

		
		//[SETUP]
		System.out.println("[Parlance v1.40 Copyright (C) 2014 - 2015 Kisora Thomas]");
		props.setProperty("annotators" , "tokenize, ssplit, pos, lemma, parse, sentiment, ner");
		
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		Response previous = null;
		
		while (!response.equals("BYE")) {
			
			
			//[QUESTION]
			System.out.println("[PARLANCE] How are you doing today? ");
			System.out.print("[USER] ");
			response = input.nextLine();
			if(response.matches("BYE")) break;
			
			
			//[ANNOTATION]
			annotation = pipeline.process(response);
			out = new PrintWriter(System.out);
			List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
            pipeline.annotate(annotation);
            
            
			//[RESPONSE]
			//System.out.println("[INFO] Generating Response object...");
			Response r = new Response(response);
			
			for (CoreMap sentence : sentences) {
				
				
				//[SENTENCE]
				//System.out.println("[INFO] Generating Sentence object...");
				Sentence s = new Sentence(sentence.toString());
				
				
				//[SENTIMENT]
				//System.out.println("[INFO] Generating Sentiment...");
				String sentiment = sentence.get(SentimentCoreAnnotations.ClassName.class);
				s.sentiment = (sentiment.toLowerCase());
				System.out.println("[PARLANCE] That sounded "+s.sentiment.toLowerCase()+".");
				
				
				//[PARTS OF SPEECH]
				PartsOfSpeech.tag(sentence.toString(),s);	
				
				
			    //[TOKENIZATION]
				for (CoreMap token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
					ArrayCoreMap aToken = (ArrayCoreMap) token;
					s.tokens.add(aToken.toShorterString());
				}
				Tokenize.Setup(s.tokens,s);
			
				
				//[DEPENDENCIES]
				SemanticGraph graph = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
				Syntax.syntaxAnalysis(graph.toString("plain"),s);
				
				
				//[CONVERT]
				Converter.generatePOS(s);
				s.setFocus();
				s.transpose();
				s.checkNegation();
				Interrogative.setQuestionType(s);
				Finale.choix(s);
				//Transformation.transformAll(s);
				r.sentences.add(s);
				c.setMood(s.sentiment);
			}
			
			
			//[CONVERSATION]
			if(!(previous==null)){
				c.deltas.add(new Delta(r,previous));
			}
			c.responses.add(r);
			c.chatLength++;
			
			previous = r;
		}
		
		
		//[GOODBYE]
		System.out.println("[PARLANCE] "+Dialogs.betterGoodbye());
		System.out.println("[PARLANCE] Until we speak again.");
	}
	
}

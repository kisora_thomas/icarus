package Objects;
import java.util.Vector;


public class Delta {

	public Vector<Tuple> deltaDependency = new Vector<Tuple>();
	public Vector<Tuple> deltaRoot = new Vector<Tuple>();
	public Vector<Dependency> newDependency1 = new Vector<Dependency>();
	public Vector<Dependency> newDependency2 = new Vector<Dependency>();
	
	public Delta(Response r1, Response r2){
		
		Sentence s1 = r1.sentences.get(0);
		Sentence s2 = r2.sentences.get(0);
		
		for(Dependency d1: s1.syntax){
			for(Dependency d2: s2.syntax){
				if(d1.type.matches(d2.type)){
					this.deltaDependency.add(new Tuple(d1,d2));	
					if(d1.type.matches("root")){
						System.out.println("I've located the Delta roots.");
						this.deltaRoot.add(new Tuple(d1,d2));
					}
				}
			}	
		}
		
		for(Dependency d1: s1.syntax){
			if(!s2.syntax.contains(d1)){
				this.newDependency1.add(d1);
			}
		}
		
		for(Dependency d2: s2.syntax){
			if(!s1.syntax.contains(d2)){
				this.newDependency2.add(d2);
			}
		}

	}
	
}

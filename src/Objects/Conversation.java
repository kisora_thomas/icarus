package Objects;
import java.util.Vector;


public class Conversation {

	public static Vector<Response> responses = new Vector<Response>();
	Vector<String> topics = new Vector<String>();
	public static Vector<Delta> deltas = new Vector<Delta>();
	public static int chatLength = 0; 
	public static int chatMood = 0; // 2,1,0,-1,-2
	public static String chatFocus = "you"; //you, me, him, her, them, it, that/this. "This conversation is about +chatFocus+."
	
	
	public static void setMood(String sentiment){
		if(sentiment.matches("very positive")){
			chatMood += 2;
		}
		else if(sentiment.matches("positive")){
			chatMood += 1;
		}
		else if(sentiment.matches("neutral")){
			chatMood += 0;
		}
		else if(sentiment.matches("negative")){
			chatMood += -1;
		}
		else if(sentiment.matches("very negative")){
			chatMood += -2;
		}
		System.out.println("[CONVERSE] This conversation as a whole is: "+chatMood);
		
	}
	
	
}

package Objects;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Algorithm.Converter;
import Algorithm.PartsOfSpeech;
import Models.Interrogative;
import Models.Model;


public class Sentence {

	public String sentence;
	public String transposition = "";
	public Vector<String> tokens = new Vector<String>();
	public Vector<String> organizations = new Vector<String>();
	public Vector<String> names = new Vector<String>();
	public Vector<String> hypernyms = new Vector<String>();
	public Vector<String> hyponyms = new Vector<String>();
	public String sentiment;
	public Vector<PartOfSpeech> pos = new Vector<PartOfSpeech>();
	public Vector<Dependency> syntax = new Vector<Dependency>();
	public Vector<PartOfSpeech> verbs = new Vector<PartOfSpeech>();
	public Vector<PartOfSpeech> nouns = new Vector<PartOfSpeech>();
	public Vector<PartOfSpeech> adjectives = new Vector<PartOfSpeech>();
	public Vector<PartOfSpeech> pronouns = new Vector<PartOfSpeech>();
	public String posPattern="";
	public String subject;
	public String verb;
	public String object;
	public String adj;
	public String type; //Declarative, Interrogative, Exclamatory, Imperative
	public Vector<Sentence> responses = new Vector<Sentence>();
	public String focus; //You, him, her, me, I, them, they, one, we, things, it, that. 
	public String[] elements;
	public String questionType;
	public String negation;
	
	public Sentence(String response) {
		this.sentence = response;
		this.elements = response.split(" ");
	}
	
	public void generatePOSPattern(){
		for(PartOfSpeech p: pos){
			posPattern.concat(p.pos).concat(" ");
		}
		System.out.println("[INFO] The sentence's POS Pattern is: "+posPattern);
	}
	
	public void addResponse(String s) throws Throwable{
		Sentence r = new Sentence(s);
		PartsOfSpeech.tag(s, r);
		Converter.generatePOS(r);
		responses.add(r);
	}
	
	private void printData() {
		System.out.println("Dependencies :"+syntax.size());
		System.out.println("Verbs"+verbs.size());
		System.out.println("Nouns"+nouns.size());
		System.out.println("Tokens"+tokens.size());		
	}
	
	public void setFocus(){
		
		Pattern verbPattern = Pattern.compile("(prp)|(prp\\$)");
		for(PartOfSpeech pos:pos){
			//System.out.println("[Debug] Root: "+pos.root +" and Part: "+ pos.pos);
			Matcher pronounMatcher = verbPattern.matcher(pos.pos.toLowerCase());
			if(pronounMatcher.matches()){
				pronouns.add(pos);
				focus = pos.root;
				System.out.println("[PARLANCE] Detected the sentence focus: "+focus);
				if(focus.matches("I") || focus.matches("me") || focus.matches("my")){
					System.out.println("[PARLANCE] So this is all about YOU?");
				}
				else if(focus.toLowerCase().matches("your") || focus.toLowerCase().matches("you")){
					System.out.println("[PARLANCE] So this is all about ME?");
				}
			}
		}
	}
	
	public void transpose(){
		for(int i = 0; i<elements.length;i++){
			if(elements[i].matches("I")){
				transposition = transposition.concat("you ");
			}
			else if(elements[i].toLowerCase().matches("my")){
				transposition = transposition.concat("your ");
			}
			else if(elements[i].toLowerCase().matches("your")){
				transposition = transposition.concat("my ");
			}
			else if(elements[i].toLowerCase().matches("you")){
				transposition = transposition.concat("I ");
			}
			else if(elements[i].toLowerCase().matches("was")){
				transposition = transposition.concat("were ");
			}
			else if(elements[i].toLowerCase().matches("am")){
				transposition = transposition.concat("are ");
			}
			else if(elements[i].toLowerCase().matches("are")){
				transposition = transposition.concat("am ");
			}
			else{
				transposition = transposition.concat(elements[i].toLowerCase()+" ");
			}
		}
		System.out.println("[PARLANCE] So, "+transposition);
	}
	
	public void checkNegation(){
		for(Dependency d : syntax){
			if(d.type.matches("neg")){
				negation = d.dependent;
				System.out.println("[PARLANCE] Negation detected: "+negation);
			}
		}
		
	}
}



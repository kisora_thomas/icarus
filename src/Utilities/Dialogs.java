package Utilities;

import Objects.Conversation;

public class Dialogs {
	
	
	
	public static String betterGoodbye(){
		String bye = "";
		
		if(Conversation.chatLength==0){
			bye = "Dude. You're leaving before we even started! Bye then!";
		}
		else if(Conversation.chatLength==1){
			bye = "Whoa, you're leaving now? We just started.";
		}
		else if(Conversation.chatLength==2){
			bye = "Yeah, I've got things to do too. Later.";
		}
		else if(Conversation.chatLength==3){
			bye = "Okay bye! See ya later!";
		}
		else if(Conversation.chatLength==4){
			bye = "Just when I was starting to know you. Haha! Bye!";
		}
		else if(Conversation.chatLength==5){
			bye = "I hope to hear from you again soon, fella!";
		}
		else if(Conversation.chatLength==6){
			bye = "I'll be thinking about what you said.";
		}
		else if(Conversation.chatLength<6){
			bye = "That was a good chat. See you later!";
		}

		return bye;
	}
	

}

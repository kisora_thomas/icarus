import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.Vector;

public class Palindrone {

	public static Vector<int[]> dates = new Vector<int[]>();
	public static Vector<int[]> reversedDates = new Vector<int[]>();

	public static void main(String[] args) throws ParseException {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Welcome to Palindrone app.");
		System.out.println("To begin, enter a year, and I will calculate the number of palindrones for that year.");
		String year = input.nextLine();
		String startDate = "01.00.";
		startDate = startDate.concat(year);
		String endDate = "12.31.2100";
		//endDate = endDate.concat(year);
		getAllDates(startDate, endDate);

	}

	public static void getAllDates(String startString, String endString) throws ParseException {

		GregorianCalendar gCal = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("MM.dd.yyyy");
		Date startDate = sdf.parse(startString);
		Date endDate = sdf.parse(endString);
		gCal.setTime(startDate);
		
		while (gCal.getTime().before(endDate)) {
			gCal.add(Calendar.DAY_OF_YEAR, 1);

			String nextDay = new SimpleDateFormat("MM.dd.yyyy").format(gCal.getTime());

			int[] original = getDateInteger(nextDay);
			int[] reversed = reverseArray(getDateInteger(nextDay));
			
			if (compareArrays(original,reversed)) {
				System.out.println("Found a palindrone: "+ Arrays.toString(original));
			}
		}
	}

	public static int[] getDateInteger(String date) {
		String processedDate = date.replace(".", "");
		char[] dateCharacters = processedDate.toCharArray();
		int[] intArray = new int[8];
		for (int i = 0; i < 8; i++) {
			intArray[i] = charToInt(dateCharacters[i]);
		}
		return intArray;
	}

	public static int charToInt(char c){
		return Integer.parseInt(String.valueOf(c));
	}
	
	public static int[] reverseArray(int[] date) {
		int[] reverse = date;
		for (int left = 0, right = date.length - 1; left < right; left++, right--) {
			int temp = reverse[left];
			reverse[left] = reverse[right];
			reverse[right] = temp;
		}
		return reverse;
	}

	public static boolean compareArrays(int[] array1, int[] array2) {
        boolean isIdentical = true;
        if (array1 != null && array2 != null){
          if (array1.length != array2.length){
              isIdentical = false;
          }
          else
              for (int i = 0; i < array2.length; i++) {
                  if (array2[i] != array1[i]) {
                      isIdentical = false;    
                  }                 
            }
        }else{
          isIdentical = false;
        }
        return isIdentical;
    }	
}
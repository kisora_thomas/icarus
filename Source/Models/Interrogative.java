package Models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Brain.CentralCorpus;
import Objects.PartOfSpeech;
import Objects.Sentence;

public class Interrogative extends Model{
	
	//How to handle questions directed at the computer
	
	/* [BASES]
	 * Who ---> Do I look like Facebook to you?
	 * What --> Ask an encyclopedia.
	 * When --> Time is really just an illusion. 
	 * Where --> There's this really cool site called GOOGLE MAPS.
	 * Why --> Hold on, I'm putting on my philosopher's hat for this. 
	 * How --> It is done with <ADV>.
	 * Which --> Pick a number one through ten. There's your answer. 
	 * 
	 * [MODIFIERS]
	 * Who<WP>: was<VBD>/is<VBZ> that<DT>/this<DT>, were<VBD> you<PRP> <VBP>, am<VBP> I<PRP>, are<VBP> you<PRP>, are<VBP> they<PRP>/those<DT>? 
	 * What<WP>: was/is that/this, were you <VBP>, am I, are you, are they? 
	 * When<WRB>:  was/is that/this, were you <VBP>, am I, are you, are they? 
	 * Where<WRB>: was/is that/this, were you <VBP>, am I, are you, are they? 
	 * Why<WRB>: was/is that/this, were you <VBP>, am I, are you, are they? 
	 * How<WRB>: is that <VBD/VBN/NN>
	 * Which<WP>: was/is that/this, were you <VBP>, am I, are you, are they? 
	 * 
	 * [ALTERNATES]
	 * Are<VBP> <PN> <VB>
	 * Do<VBP> <PN> <VB>
	 * Does<NNP> <PN> <VB> -->
	 * Have<VBP> <PN> <VB> --> I can't say right away. *Backwards reference
	 * Has<VBZ> <PRP> <VBN> --> 
	 * May<NNP> <PN> <VB> --> I give you my permission.
	 * Would<MD> <PN>  <VB>
	 * Can<MD> <PN> <VB>
	 * Could<MD> <PRP> <VB>
	 * Should<MD> <PRP> <VB>
	 * */
	
	public static void setQuestionType(Sentence s){                 
            s.questionType = "";                                                                                                  
			if(s.elements[0].toLowerCase().matches("who")){s.questionType = ("who");}
			else if(s.elements[0].toLowerCase().matches("what")){s.questionType = ("what");} 
			else if(s.elements[0].toLowerCase().matches("when")){s.questionType = ("when");} 
			else if(s.elements[0].toLowerCase().matches("where")){s.questionType = ("where");} 
			else if(s.elements[0].toLowerCase().matches("why")){s.questionType = ("why");} 
			else if(s.elements[0].toLowerCase().matches("how")){s.questionType = ("how");} 
			else if(s.elements[0].toLowerCase().matches("which")){s.questionType = ("which");}
			
			if(!(s.questionType=="")){
				CentralCorpus.CorpusPrint("[INFO] Question Type: " +s.questionType);
			}
	}
	
	
	

}

package Models;

import Brain.CentralCorpus;
import Objects.Sentence;

public class Exclamatory {

	public static void simpleReply(Sentence s){
		if(s.type.matches("exclamatory")){
			if(s.numericalSentiment()>0){
				CentralCorpus.CorpusPrint("[ICARUS-] Yay! I am really excited for "+s.transposedFocus+"!");
			}
			else if(s.numericalSentiment()<0){
				CentralCorpus.CorpusPrint("[ICARUS-] Do not yell at me like that!");
				
			}			
		}
	}
}

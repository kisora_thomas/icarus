package Models;

import Brain.CentralCorpus;
import Objects.Sentence;
import Utilities.WordNet;

public class Imperative {
	
	/* The Imperative class is used when the user issues a command to the machine. 
	 * It's up to the computer to know how to respond accordingly, provided it's in its power.
	 * When combined with sentiment, the machine's responses can also indicate whether it will follow the order.
	 * Looks like 'please' really does go a long way.
	 * 
	 * */
	
	
	public static void simpleReply(Sentence s){
		if(s.type.matches("imperative")){
			if(s.numericalSentiment()>0){
				CentralCorpus.CorpusPrint("[ICARUS-] I'd be "+WordNet.getSimilar("happy")+" to oblige.");
				//Okay, how do I do that?
				//I'd love to.
				//How do I start?
			}
			else if(s.numericalSentiment()<0){
				CentralCorpus.CorpusPrint("[ICARUS-] Next time, ask nicely.");
				//I don't take orders from you.
				//I don't listen to meanies.
			}
		}
	}
	

	
	
	
}

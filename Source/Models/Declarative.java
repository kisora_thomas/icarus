package Models;

import java.util.Random;

import Brain.CentralCorpus;
import Objects.Sentence;
import Objects.Word;
import Utilities.WordNet;

public class Declarative {

	/*I suppose; I think; I presume; Considering; All things considered; If you think about it, it seems; It seems;
	 * Hmm...; I guess; Looks like; Perhaps; Conversely; 
	 * 
	 * HUMAN: I think...; COMP: I agree; That's understandable; That makes sense; I disagree, but; 
	 * HUMAN: I feel...; COMP: I feel the same; Lots of people feel that way; That's true, but; That's tough;
	 * HUMAN: I like...; COMP: 
	 * HUMAN: I was 
	 * 
	 * */
	
	//IMPROVE: use similar words to 'good', and 'dissappointing', and use similar phrases for 'to hear'. 
	public static void simpleReply(Sentence s){
		if(s.type.matches("declarative")){
			if(s.numericalSentiment()>0){
				CentralCorpus.CorpusPrint("That's "+WordNet.getSimilar("good")+" to hear.");
			}
			else if(s.numericalSentiment()<0){
				CentralCorpus.CorpusPrint("That's "+WordNet.getSimilar("bad")+" to hear.");
			}
		}
	}

	public static void detectHumanAssertion(Sentence s){
		if(s.origin.matches("human")){
			if(!s.predicate.matches("")&&!s.postdicate.matches("")){
				if(s.transposedFocus.toLowerCase().matches("me")){
					if(s.numericalSentiment()>0){
						CentralCorpus.CorpusPrint("[ICARUS] I am happy to be "+s.postdicate+"!");
						//Improve with array of assertion responses 
					}
					else if(s.numericalSentiment()==0){
						CentralCorpus.CorpusPrint("[ICARUS] Should I be "+s.postdicate+"?");
					}
					else if(s.numericalSentiment()<0){
						CentralCorpus.CorpusPrint("[ICARUS] I don't want to be "+s.postdicate+"?");
					}
				}
			}
		}
	}

	public static void listSimilarities(Word w){
		String similarities="";
		for(int i=0;i<w.similar.length;i++){
			similarities = similarities.concat(w.similar[i]+", ");
		}
	}
	
	public static String captializeFirstLetter(String input){
		return input.substring(0, 1).toUpperCase() + input.substring(1);
	}
	
	
	
}

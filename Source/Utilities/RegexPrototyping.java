package Utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexPrototyping {

	public static void main(String[] args) {
		String test1 = "nsubj cop advmod root ";
		String test2 = "nsubj cop advmod det root";
		String test3 = "nsubj aux cop advmod root";
		
		//"(discourse)? nsubj (aux)? cop advmod \\.*"
		Pattern humanAssertionSyntaxPattern = Pattern.compile("(discourse)? ?(nsubj) (aux)? ?(cop)");
		Matcher m = humanAssertionSyntaxPattern.matcher(test3);
		
		if(m.find()){
			System.out.println("Success.");
			System.out.println(m.group());
		}

	}

}

package Utilities;

import java.util.Random;
import java.util.HashMap;
import java.util.Map;

import Brain.CentralCorpus;
import rita.wordnet.RiWordnet;
import rita.RiTa;

public class WordNet {

	public static RiWordnet wordnet = new RiWordnet(null);

	public static String pickReplacement(String word) {
		if (word != null) {

			String pos = wordnet.getBestPos(word);
			if (pos != null) {
				// First check and see if there are any antonyms
				// If so, pick a random one and return
				String[] antonyms = wordnet.getAllAntonyms(word, pos);
				if (antonyms != null) {
					String replacement = antonyms[(int) Math.random() * antonyms.length];
					CentralCorpus.CorpusPrint(word + " ==> " + replacement + "  (antonym " + pos + ")");
					return replacement;
				}
				// Then check and see if there are any hyponyms
				// If so, pick a random one and return
				String[] hyponyms = wordnet.getAllHyponyms(word, pos);
				if (hyponyms != null) {
					String replacement = hyponyms[(int) Math.random() * hyponyms.length];
					CentralCorpus.CorpusPrint(word + " ==> " + replacement + "  (hyponym " + pos + ")");
					return replacement;
				}
				// Same thing for synonyms
				String[] synonyms = wordnet.getAllSynonyms(word, pos);
				if (synonyms != null) {
					String replacement = synonyms[(int) Math.random() * synonyms.length];
					CentralCorpus.CorpusPrint(word + " ==> " + replacement + "  (synonym " + pos + ")");
					return replacement;
				}
			}
		}
		return word;
	}
	
	public static String getHypernym(String word){
		String[] hypernyms;
		try {
			hypernyms = WordNetPrototyping.getHypernyms(word);
			String hypernym = (hypernyms[new Random().nextInt(hypernyms.length)]);
			return hypernym;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return "";
		
	}
	

	public static void autoConjugation(){}
	
	public static String conjugate(String dependent){
		Map args = new HashMap();
		args.put("tense", RiTa.PRESENT_TENSE);
		args.put("number", RiTa.SINGULAR);
		args.put("person", RiTa.FIRST_PERSON);
		return RiTa.conjugate(dependent, args);

	}
	
	
	public static String getSimilar(String word) {
		String pos = wordnet.getBestPos(word);
		if(!(pos==null)){
			String similarSet[] = wordnet.getAllSimilar(word, pos);
			String choice = (similarSet[new Random().nextInt(similarSet.length)]);
			return choice;
		}
		return "null";
	}

	public static String getHyponym(String word) {
		String[] hyponyms;
		try {
			hyponyms = WordNetPrototyping.getHyponyms(word);
			String hyponym = (hyponyms[new Random().nextInt(hyponyms.length)]);
			return hyponym;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return "";
		
	}

	public static String getHyponym(String word, String wnPOS) {
		String[] hyponyms;
		try {
			hyponyms = WordNetPrototyping.getHyponyms(word,wnPOS);
			String hyponym = (hyponyms[new Random().nextInt(hyponyms.length)]);
			return hyponym;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return "";
	}
}

package Utilities;

import java.util.Comparator;

import Objects.Dependency;

public class SyntaxComparator implements Comparator<Dependency>{

    public int compare(Dependency d1, Dependency d2) {
    	return Integer.compare(d1.dependentIndex,d2.dependentIndex);
    }

}

package Objects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Utilities.SyntaxComparator;
import Utilities.WordNet;
import Algorithm.Converter;
import Algorithm.PartsOfSpeech;
import Brain.CentralCorpus;
import Brain.SelfKnowledge;

public class Sentence {

	public String sentence="";
	public String origin="";
	public String transposition = "";
	public String transposedFocus = "you";
	public String sentiment="";
	public String strength = ""; //Find a way to calculate this. Potentially machine trainable. 	
	public String posPattern="";
	public String subject="";
	public String verb="";
	public String object="";
	public String adj="";
	public String type=""; //Declarative, Interrogative, Exclamatory, Imperative
	public String questionType="";	
	public String focus=""; //You, him, her, me, I, them, they, one, we, things, it, that. 
	public String tenseString="";
	public String syntaxPattern="";
	public String predicate ="";
	public String postdicate="";
	public ArrayList<String> regions = new ArrayList<String>();
	public ArrayList<Word> words = new ArrayList<Word>();
	public ArrayList<String> tokens = new ArrayList<String>();
	public ArrayList<String> organizations = new ArrayList<String>();
	public ArrayList<String> names = new ArrayList<String>();
	public Vector<PartOfSpeech> pos = new Vector<PartOfSpeech>();
	public ArrayList<Dependency> syntax = new ArrayList<Dependency>();
	public Vector<PartOfSpeech> verbs = new Vector<PartOfSpeech>();
	public Vector<PartOfSpeech> nouns = new Vector<PartOfSpeech>();
	public Vector<PartOfSpeech> adjectives = new Vector<PartOfSpeech>();
	public Vector<PartOfSpeech> pronouns = new Vector<PartOfSpeech>();
	public Vector<Sentence> responses = new Vector<Sentence>();
	public String[] elements;
	public Dependency negation;
	public Dependency possession;
	public Dependency nominalSubject;
	public Tense tense = Tense.BASE_FORM;
	
	public enum Tense {
		BASE_FORM,PAST_TENSE,PRESENT_PARTICIPLE,
		PAST_PARTICIPLE,PRESENT_TENSE,PT3PS
	}
	
	public Sentence(String response, String origin) {
		this.sentence = response;
		this.origin = origin;
		this.elements = response.split(" ");
		for(String e: elements){
			Word w = new Word(e.toLowerCase().replace(",", "").replace(".", ""));
			words.add(w);
		}
	}
	
	public void setRegions(){

		regions = (ArrayList<String>) Arrays.asList(sentence.split(","));
		//Detect words at start of sentence, like interjections
		//Also, check for listing usage of commas.	
	}
	
	public void populateWords(){
		for(int i =0;i<words.size();i++){
			Word w = words.get(i);
			w.pos = pos.get(i);
			//w.syntax = syntax.get(i); //DEBUG THIS. The roots have a tendency to come first. :/
			w.sentiment = w.getSentiment(); //Call from CentralCorpus
			w.hypernyms = w.getHypernyms();
			w.holonyms = w.getHolonyms();
			w.synonyms = w.getSynonyms();
			w.antonyms = w.getAntonyms();
			w.similar = w.getSimilar();
			w.examples = w.getExamples();
			w.questionType = w.setQuestionType();
		}
	}
	
	public void sortSyntax() {
		  SyntaxComparator comparator = new SyntaxComparator();
		  Collections.sort(syntax, comparator);
	}
	
	public void generatePOSPattern(){
		for(PartOfSpeech p: pos){
			posPattern.concat(p.type).concat(" ");
		}
		CentralCorpus.CorpusPrint("[INFO] The sentence's POS Pattern is: "+posPattern);
	}
	
	public void addResponse(String s) throws Throwable{
		Sentence r = new Sentence(s,"Icarus");
		PartsOfSpeech.tag(s, r);
		Converter.generatePOS(r);
		responses.add(r);
	}
	
	private void printData() {
		CentralCorpus.CorpusPrint("Dependencies :"+syntax.size());
		CentralCorpus.CorpusPrint("Verbs "+verbs.size());
		CentralCorpus.CorpusPrint("Nouns "+nouns.size());
		CentralCorpus.CorpusPrint("Tokens "+tokens.size());		
	}
	
	public void extractTense(){		
		
		for(Dependency d : syntax){
			if(d.type.matches("vb")){
				tense = Tense.BASE_FORM;
			}
			else if(d.type.matches("vbd")){
				tense = Tense.BASE_FORM;
			}
			else if(d.type.matches("vb")){
				tense = Tense.BASE_FORM;
			}
			else if(d.type.matches("vb")){
				tense = Tense.BASE_FORM;
			}
			else if(d.type.matches("vb")){
				tense = Tense.BASE_FORM;
			}
			else if(d.type.matches("vb")){
				tense = Tense.BASE_FORM;
			}	
		}
	}
	
	public void syntaxPatternToString(){
		for(Dependency d:syntax){
			syntaxPattern = syntaxPattern.concat(d.type).concat(" ");
		}
	};
	
	public void extractDicates(){
		
		System.out.println("\n[DEBUG] SyntaxPattern: "+syntaxPattern);
		Pattern predicatePattern = Pattern.compile("(discourse )?(nsubj )(aux )?(cop)");
		Matcher m = predicatePattern.matcher(syntaxPattern);
		String matchedElements;
		if(m.find()){
			matchedElements = m.group();
			System.out.println(matchedElements);
			boolean predicateFlag=true;
			for(Dependency d:syntax){
				
				if(predicateFlag){
					if(!d.type.matches("discourse")){
					predicate = predicate.concat(d.dependent+" ");
					}
				}
				if(!predicateFlag){
					postdicate = postdicate.concat(d.dependent+" ");
				}
				if(d.type.matches("cop")){
					predicateFlag = false;
				}
			}
			CentralCorpus.CorpusPrint("[INFO] Predicate is: "+predicate);
			CentralCorpus.CorpusPrint("[INFO] Postdicate is: "+postdicate);	
		}		
	}
	
	public void setStrength(){
		//Check for weak words
		//Check for strong words
		
	}
	
	public void setPronounFocus(){

		//Improve by creating an ArrayList of pronoun foci. 

		if(origin.matches("human")){
			Pattern verbPattern = Pattern.compile("(prp)|(prp\\$)|(dt)");
			for(PartOfSpeech pos:pos){
				//CentralCorpus.CorpusPrint("[Debug] Root: "+pos.root +" and Part: "+ pos.pos);
				Matcher pronounMatcher = verbPattern.matcher(pos.type.toLowerCase());
				if(pronounMatcher.matches()){
					pronouns.add(pos);
					focus = pos.root;
					CentralCorpus.CorpusPrint("[INFO] Detected the pronoun focus: "+focus);
					if(focus.matches("I") || focus.matches("me") || focus.matches("my")){
						transposedFocus = "you";
						CentralCorpus.CorpusPrint("[INFO] So this is all about "+transposedFocus+"?");
					}
					else if(focus.toLowerCase().matches("your") || focus.toLowerCase().matches("you")){
						transposedFocus = "me";
						CentralCorpus.CorpusPrint("[INFO] So this is all about "+transposedFocus+"?");
					}
					else if(focus.toLowerCase().matches("him") || focus.toLowerCase().matches("his")){
						CentralCorpus.CorpusPrint("[INFO] So this is all about a GUY?");
					}
					else if(focus.toLowerCase().matches("her") || focus.toLowerCase().matches("hers")||focus.toLowerCase().matches("she")){
						CentralCorpus.CorpusPrint("[INFO] So this is all about a GIRL?");
					}
					else if(focus.toLowerCase().matches("this") || focus.toLowerCase().matches("that") || focus.toLowerCase().matches("it")){
						CentralCorpus.CorpusPrint("[INFO] So this is all about IT/THIS/THAT?");
					}
					else if(focus.toLowerCase().matches("them") || focus.toLowerCase().matches("they")){
						CentralCorpus.CorpusPrint("[INFO] So this is all about OTHERS?");
					}
					else if(focus.toLowerCase().matches("we") || focus.toLowerCase().matches("us")){
						CentralCorpus.CorpusPrint("[INFO] So this is all about the GROUP?");
					}
				}
			}
		}
	}

	public void transpose(){
		if(origin.matches("human")){
			for(int i = 0; i<elements.length;i++){
				if(elements[i].matches("I")){
					transposition = transposition.concat("you ");
					words.get(i).transposition = "you";
				}
				else if(elements[i].toLowerCase().matches("my")){
					transposition = transposition.concat("your ");
					words.get(i).transposition = "your";
				}
				else if(elements[i].toLowerCase().matches("your")){
					transposition = transposition.concat("my ");
					words.get(i).transposition = "my";
				}
				else if(elements[i].toLowerCase().matches("you")){
					transposition = transposition.concat("I ");
					words.get(i).transposition = "I";
				}
				else if(elements[i].toLowerCase().matches("was") && focus.matches("I")){
					transposition = transposition.concat("were ");
					words.get(i).transposition = "were";
				}
				else if(elements[i].toLowerCase().matches("am")){
					transposition = transposition.concat("are ");
					words.get(i).transposition = "are";
				}
				else if(elements[i].toLowerCase().matches("are") && !(isCopula("are"))){
					transposition = transposition.concat("am ");
					words.get(i).transposition = "am";
				}
				else{
					transposition = transposition.concat(elements[i].toLowerCase()+" ");
				}
			}
			CentralCorpus.CorpusPrint("[ICARUS-] So, "+transposition);
		}
	}
	
	public boolean isCopula(String dep){
		for(Dependency d : syntax){
			if(d.dependent.matches(dep) && (d.type.matches("cop"))){
				return true;
			}
		}
		return false;
	}
	
	public void detectNegation(){
		for(Dependency d : syntax){
			if(d.type.matches("neg")){
				negation = d;
				CentralCorpus.CorpusPrint("[INFO] Negation detected: "+negation.dependent);
			}
		}
	}
	
	public void detectPossession(){
		for(Dependency d : syntax){
			if(d.type.matches("poss")){
				possession = d;
				for(Dependency d2:syntax){
					if(d2.type.matches("of") && d2.governor.matches(possession.governor)){
						possession.governor = d2.dependent;
					}
				}
				CentralCorpus.CorpusPrint("[INFO] Possession detected: "+possession.governor);
				CentralCorpus.CorpusPrint("[ICARUS-] So, the "+possession.governor+" belongs to "+transposedFocus+"?");
				if(transposedFocus.matches("ME")){
					for(String key : SelfKnowledge.selfKnowledge.keySet()){
						if(key.matches(possession.governor)){
							CentralCorpus.CorpusPrint("[ICARUS-] I know something about that, my "+WordNet.conjugate(possession.governor)+" is "+SelfKnowledge.selfKnowledge.get(key)+".");
						}
					}
				}
			}
		}		
	}
	
	public void detectSubject(){
		for(Dependency d : syntax){
			if(d.type.matches("nsubj")){
				nominalSubject = d;
				CentralCorpus.CorpusPrint("[INFO] Subject detected: "+nominalSubject.dependent);
			}
		}		
	}
	
	public int numericalSentiment(){
		int sentimentInt = 0;
		if(sentiment.matches("very positive")){
			sentimentInt += 2;
		}
		else if(sentiment.matches("positive")){
			sentimentInt += 1;
		}
		else if(sentiment.matches("neutral")){
			sentimentInt += 0;
		}
		else if(sentiment.matches("negative")){
			sentimentInt += -1;
		}
		else if(sentiment.matches("very negative")){
			sentimentInt += -2;
		}
		CentralCorpus.CorpusPrint("[DEBUG] "+sentimentInt);
		return sentimentInt;
	}
	
	
	public void printDependencies(){
		for(Dependency d1: syntax){
			CentralCorpus.CorpusPrint(d1.toString());
		}
	}
}



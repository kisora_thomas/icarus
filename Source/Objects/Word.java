package Objects;

import java.util.ArrayList;

import Utilities.WordNet;

public class Word {
	
	public String original;
	public String transposition;
	public String wnPOS;	
	public String sentiment;
	public String questionType;
	public Dependency syntax;
	public PartOfSpeech pos;
	public String[] hypernyms;
	public String[] holonyms;
	public String[] hyponyms;
	public String[] synonyms;
	public String[] antonyms;
	public String[] similar;
	public String[] examples;
	
	public Word(String w){
		this.original = w;
		wnPOS = WordNet.wordnet.getBestPos(w);
		//CentralCorpus.CorpusPrint("[DEBUG] "+wnPOS);
	}

	public String getSentiment() {
		// TODO Auto-generated method stub
		return null;
	}

	public String[] getHypernyms() {
		if(!(wnPOS==null)){
			return WordNet.wordnet.getAllHypernyms(original, wnPOS);
		}
		else{
			return null;
		}
	}

	public String[] getHolonyms() {
		if(!(wnPOS==null)){
			return WordNet.wordnet.getAllHolonyms(original, wnPOS);
		}
		else{
			return null;
		}
	}

	public String[] getHyponyms() {
		if(!(wnPOS==null)){
			return WordNet.wordnet.getAllHyponyms(original, wnPOS);
		}
		else{
			return null;
		}
	}
	
	public String[] getSynonyms() {
		if(!(wnPOS==null)){
			return WordNet.wordnet.getAllSynonyms(original, wnPOS);
		}
		else{
			return null;
		}
	}


	public String[] getAntonyms() {
		if(!(wnPOS==null)){
			return WordNet.wordnet.getAllAntonyms(original, wnPOS);
		}
		else{
			return null;
		}
	}
	
	public String[] getExamples() {
		if(!(wnPOS==null)){
			return WordNet.wordnet.getAllExamples(original, wnPOS);
		}
		else{
			return null;
		}
	}
	
	public String[] getSimilar() {
		if(!(wnPOS==null)){
			return WordNet.wordnet.getAllSimilar(original, wnPOS);
		}
		else{
			return null;
		}
	}
	
	public void capitalizeProperNoun(){
		
	} 


	public String setQuestionType() {
		if(pos.type.matches("NN")||pos.type.matches("NNS")||pos.type.matches("VBD")||pos.type.matches("JJ")){
			questionType = "what";
		}
		else if(pos.type.matches("RB")){
			questionType = "how";
		}
		else if(pos.type.matches("NNP") || pos.type.matches("PRP")){
			questionType = "who";
		}
		else if(pos.type.matches("PRP\\$")){
			questionType = "whose";
		}
		else if(pos.type.matches("PDT")||pos.type.matches("DT")){
			questionType = "which";
		}
		else if(pos.type.matches("RBR") ||pos.type.matches("RBS")||pos.type.matches("JJR")){
			questionType = "compared to what";
		}
		else if(pos.type.matches("EX")||pos.type.matches("IN")){
			questionType = "where";
		}
		else if(pos.type.matches("FW")){
			questionType = "which language";
		}
		return questionType;
	}

}

package Brain;

import java.util.concurrent.TimeUnit;
import java.util.*;
import java.io.*;

import Algorithm.Converse;
import Algorithm.Converter;
import Algorithm.PartsOfSpeech;
import Algorithm.QuestionAnswer;
import Algorithm.Syntax;
import Algorithm.Tokenize;
import Models.Declarative;
import Models.Exclamatory;
import Models.Imperative;
import Models.Interrogative;
import Objects.Conversation;
import Objects.Delta;
import Objects.Response;
import Objects.Sentence;
import Utilities.Dialogs;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.util.ArrayCoreMap;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.*;
import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations;
import edu.stanford.nlp.io.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.util.*;

public class CentralCorpus {

	/* From this point on, the Central Corpus is responsible for taking in all of Icarus's replies
	 * For further analyis. Considering the machine needs to select one or two as a reply. 
	 * */
	
	public static boolean printIcarus = true;
	public static boolean printInfo = true;
	public static boolean printDebug = false;
	public static long printSpeed=25;
	public static Properties props = new Properties();
	public static StanfordCoreNLP pipeline;
	public static Annotation humanAnnotation;
	public static Annotation IcarusAnnotation;
	public static Conversation c = new Conversation();
	public static Response previousResponse = null;
	public static Response currentResponse = null;
	
	public static void SetupBrain(){
		props.setProperty("annotators" , "tokenize, ssplit, pos, lemma, parse, sentiment, ner");
		pipeline = new StanfordCoreNLP(props);
	}
	
	public static void CorpusPrint(String toConsider){
		String firstWord = toConsider.split(" ")[0];
//		System.out.println("[DEBUG] First word is: "+firstWord);
//		System.out.println("[DEBUG] Print ICARUS: "+(firstWord.matches("\\[ICARUS\\]")&& printIcarus));	
//		System.out.println("[DEBUG] Print INFO: "+(firstWord.matches("\\[INFO\\]")&& printInfo));
//		System.out.println("[DEBUG] Print DEBUG: "+(firstWord.matches("\\[DEBUG\\]")&& printInfo));
		
		if(firstWord.matches("\\[ICARUS\\]") && printIcarus){
			seriouslyConsider(toConsider);
		}
		else if(firstWord.matches("\\[ICARUS-\\]")){
			System.out.print("\n"+toConsider);
		}
		else if(firstWord.matches("\\[HUMAN\\]")){
			print(toConsider);
		}
		else if(firstWord.matches("\\[Icarus")){
			System.out.println(toConsider);
		}
		else if(firstWord.matches("\\[INFO\\]")&& printInfo){
			print(toConsider);
		}
		else if(firstWord.matches("\\[DEBUG\\]")&& printDebug){
			System.out.println("\n"+toConsider);
		}
	}
	
	public static void seriouslyConsider(String considering){
		String name = "[ICARUS] "; 
		considering = considering.replace(name, "");
		//[ANNOTATION]
		IcarusAnnotation = pipeline.process(considering);
		List<CoreMap> sentences =IcarusAnnotation.get(CoreAnnotations.SentencesAnnotation.class);
		pipeline.annotate(IcarusAnnotation);
		currentResponse = new Response(considering);
		
		for (CoreMap sentence : sentences) {
			Sentence s = new Sentence(sentence.toString(),"Icarus");
			s.sentiment = sentence.get(SentimentCoreAnnotations.SentimentClass.class).toLowerCase();
			try {
				PartsOfSpeech.tag(sentence.toString(),s);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			//[TOKENIZE]
			Tokenize(sentence, s);
			//[SYNTAX]
			SemanticGraph graph = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
			
			Syntax syn = new Syntax();
			syn.syntaxAnalysis(graph.toString(SemanticGraph.OutputFormat.LIST),s);
			
			//[CONVERSION]
			Converter.generatePOS(s);
			//[SENTENCE FUNCTIONS]
			callSentenceFunctions(s);
//			//[STATIC CLASSES]
			callStaticClasses(s);
//			//[RESPONSE]
			currentResponse.sentences.add(s);
		}
		//[CONVERSATION]
		if(!(previousResponse==null)){
			//CentralCorpus.c.addDelta(new Delta(previousResponse,currentResponse)); <-- Must create a Cross-Compatible Delta object
		}
		CentralCorpus.c.responses.add(currentResponse);
		previousResponse = currentResponse;
		
		//ANNOTATION COMPLETE. 
		//Now new analysis may begin.
		
		print(name.concat(considering));
	}
	
	public static void print(String data) {
		String newLine = "\n";
		newLine = newLine.concat(data);
		data = newLine;
		TimeUnit unit = TimeUnit.MILLISECONDS;
		long delay = printSpeed;
		for (char ch : data.toCharArray()) {
			System.out.print(ch);
			try {
				unit.sleep(delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void Tokenize(CoreMap sentence, Sentence s){
		for (CoreMap token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
			ArrayCoreMap aToken = (ArrayCoreMap) token;
			s.tokens.add(aToken.toShorterString());
		}
		Tokenize.Setup(s.tokens,s);
	}
	
	public static void callSentenceFunctions(Sentence s){
		s.sortSyntax();
		s.setPronounFocus();
		s.transpose();
		s.detectNegation();
		s.detectPossession();
		s.detectSubject();
		s.populateWords();
		s.syntaxPatternToString();
		s.extractTense();
		s.extractDicates();
	}
	
	public static void callStaticClasses(Sentence s){
		Interrogative.setQuestionType(s);
		Exclamatory.simpleReply(s);
		Declarative.simpleReply(s);
		Declarative.detectHumanAssertion(s);
		Imperative.simpleReply(s);
		QuestionAnswer.choix(s);
		//Transformation.transformAll(s);
	}
	
	public static void callSyntaxFunctions(CoreMap sentence, Sentence s){
		SemanticGraph graph = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
		Syntax syn = new Syntax();
		//syn.syntaxAnalysis(graph.toString("plain"),s);
	}
	
	public static void callConversationFunctions(Sentence s){
		if(!(Converse.previousResponse==null)){
			CentralCorpus.CorpusPrint("[INFO] Delta previous: "+Converse.previousResponse.sentences.get(0).sentence);
			CentralCorpus.CorpusPrint("[INFO] Delta current: "+Converse.currentResponse.sentences.get(0).sentence); 
			CentralCorpus.c.addDelta(new Delta(Converse.previousResponse,Converse.currentResponse));
		}
		CentralCorpus.c.responses.add(Converse.currentResponse);
		CentralCorpus.c.chatLength++;
		CentralCorpus.c.setConversationQuadrant();
		Converse.previousResponse = Converse.currentResponse;
		CentralCorpus.CorpusPrint(Dialogs.conversationDriver(CentralCorpus.c,s));
	}
	
	public static void redundancyAnalysis(){
		//Use the Timeline Package
		
		
	}
	
}

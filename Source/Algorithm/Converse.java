package Algorithm;

import java.util.*;

import Brain.CentralCorpus;
import Brain.SelfKnowledge;
import Objects.Response;
import Objects.Sentence;
import Utilities.Dialogs;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;

public class Converse {

	public static Scanner input = new  Scanner(System.in);
	public static String response = "";
	public static Response previousResponse = null;
    public static Response currentResponse = null;
	public static void main(String[] args) throws Throwable {
	
		//[SETUP]
		CentralCorpus.CorpusPrint("[Icarus v1.50 Copyright (C) 2014 - 2015 Kisora Thomas]");
		CentralCorpus.SetupBrain();
		SelfKnowledge.composeKnowledge();

		
		CentralCorpus.CorpusPrint("[ICARUS] How are you doing today?");
		
		while (!response.equals("BYE")) {
			
			Sentence s = new Sentence("","initialize");
			
			//[QUESTION]
			CentralCorpus.CorpusPrint("[HUMAN] ");
			response = input.nextLine();
			if(response.matches("BYE")) break;
			
			//[ANNOTATION]
			CentralCorpus.humanAnnotation = CentralCorpus.pipeline.process(response);
			List<CoreMap> sentences = CentralCorpus.humanAnnotation.get(CoreAnnotations.SentencesAnnotation.class);
			CentralCorpus.pipeline.annotate(CentralCorpus.humanAnnotation);
            
			//[RESPONSE]
			currentResponse = new Response(response);
			
			for (CoreMap sentence : sentences) {
				
				//[SENTENCE]
				s = new Sentence(sentence.toString(),"human");
				
				//[SENTIMENT]
				s.sentiment = sentence.get(SentimentCoreAnnotations.SentimentClass.class).toLowerCase();
				CentralCorpus.CorpusPrint("[ICARUS-] That sounded "+s.sentiment.toLowerCase()+".");
				
				//[PARTS OF SPEECH]
				PartsOfSpeech.tag(sentence.toString(),s);	
				
			    //[TOKENIZATION]
				CentralCorpus.Tokenize(sentence, s);
								
				//[SYNTAX]
				SemanticGraph graph = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
				Syntax syn = new Syntax();
				syn.syntaxAnalysis(graph.toString(SemanticGraph.OutputFormat.LIST),s);
				
				//[CONVERSION]
				Converter.generatePOS(s);
				
				//[SENTENCE FUNCTIONS]
				CentralCorpus.callSentenceFunctions(s);
				
				//[STATIC CLASSES]
				CentralCorpus.callStaticClasses(s);
				currentResponse.sentences.add(s);
				CentralCorpus.c.setMood(s.sentiment);
			}
			
			//[CONVERSATION]
			CentralCorpus.callConversationFunctions(s);
		}
		
		//[GOODBYE]
		CentralCorpus.CorpusPrint("[ICARUS] "+Dialogs.betterGoodbye(CentralCorpus.c));
	}
}
package Algorithm;
import java.util.*;

import Brain.CentralCorpus;
import Objects.Dependency;
import Objects.Sentence;

public class Syntax {

	public Vector<String> dependency = new Vector<String>();
	public Vector<String> depFromColl = new Vector<String>();
	public Vector<String> collapsed = new Vector<String>();
	
	public void syntaxAnalysis(String graph,Sentence s) {
		
		String[] graphA = graph.split("\n");
		
		for (int i = 0; i < graphA.length; i++) {
			dependency.add(graphA[i]);
		}

		for (int k = 0; k < dependency.size(); k++) {

			String[] temp = dependency.get(k).toString().split("\\(");
			String dependencyType = temp[0];

			if (dependencyType.contains(":")) {
				String[] temp2 = dependencyType.split(":");
				depFromColl.add(temp2[0]);
				collapsed.add(temp2[1]);
				dependencyType = temp2[0];
			}

			if (dependencyType.matches(Dependencies(dependencyType))) {
				Analysis(dependency.get(k).toString(), s);
			} else {
				break;
			}
		}
	}

	public String Dependencies(String depen) {
		String[] dependencyList = { "acomp", "advcl", "advmod", "agent",
				"amod", "appos", "aux", "auxpass", "cc", "ccomp", "conj",
				"cop", "csubj", "csubjpass", "dep", "det", "discourse", "dobj",
				"expl", "goeswith", "iobj", "mark", "mwe", "neg", "nn",
				"npadvmod", "nsubj", "nsubjpass", "num", "number", "parataxis",
				"pcomp", "pobj", "poss", "possessive", "preconj", "predet",
				"prep", "prepc", "prt", "punct", "quantmod", "referent",
				"root", "tmod", "vmod", "xcomp", "xsubj" };
		boolean flag = true;

		while (flag == true) {
			//CentralCorpus.CorpusPrint(depen);

			for (int i = 0; i < dependencyList.length; i++) {
				if (dependencyList[i].matches(depen)) {
					return dependencyList[i];
				} else {

				}
			}
			flag = false;
		}
		return depen;
	}

	public void Analysis(String dependency, Sentence s) {

		//CentralCorpus.CorpusPrint("[LAUNCH] Syntax.Analysis()");
		
		String cleanDependency = "";
		String collapsed = "";
		String[] typeVsContent = dependency.split("\\(");
		String extractedDependency = typeVsContent[0];
		String[] governorVsDependent = typeVsContent[1].split(",");
		String[] governorVsIndex = governorVsDependent[0].split("-");
		String governor = governorVsIndex[0];
		String[] dependentVsIndex = governorVsDependent[1].split("-");
		String dependent = dependentVsIndex[0];
		int index = Integer.parseInt(dependentVsIndex[1].split("\\)")[0]);
		
		Dependency d = new Dependency(extractedDependency.trim(),governor.trim(),dependent.trim(),index);
		
		if (extractedDependency.contains(":")) {
			String[] actualDependency = extractedDependency.split(":");
			cleanDependency = actualDependency[0];
			collapsed = actualDependency[1];
			CentralCorpus.CorpusPrint("[INFO] I found a collapsed dependency.");
			d = new Dependency(collapsed.trim(),governor.trim(),dependent.trim(),index);
		}
		
		s.syntax.add(d);

		if (extractedDependency.matches("root")) {
			//CentralCorpus.CorpusPrint("root");
		}
		if (extractedDependency.matches("aux")) {
			//CentralCorpus.CorpusPrint("auxillary.");	
		}
		if (extractedDependency.matches("auxpass")) {
			//CentralCorpus.CorpusPrint("passive auxilary.");			
		}
		if (extractedDependency.matches("cop")) {
			//CentralCorpus.CorpusPrint("copula");			
		}
		if (extractedDependency.matches("arg")) {
			//CentralCorpus.CorpusPrint("argument.");			
		}
		if (extractedDependency.matches("agent")) {
			//CentralCorpus.CorpusPrint("agent.");			
		}
		if (extractedDependency.matches("comp")) {
			//CentralCorpus.CorpusPrint("compliment.");			
		}
		if (extractedDependency.matches("acomp")) {
			//CentralCorpus.CorpusPrint("adjectival complement");			
		}
		if (extractedDependency.matches("ccomp")) {
			//CentralCorpus.CorpusPrint("clausal complement");			
		}
		if (extractedDependency.matches("xcomp")) {
			//CentralCorpus.CorpusPrint("clausal complement with external subject.");			
		}
		if (extractedDependency.matches("obj")) {
			//CentralCorpus.CorpusPrint("object.");			
		}
		if (extractedDependency.matches("dobj")) {
			//CentralCorpus.CorpusPrint("direct object");			
		}
		if (extractedDependency.matches("iobj")) {
			//CentralCorpus.CorpusPrint("Indirect Object");			
		}
		if (extractedDependency.matches("pobj")) {
			//CentralCorpus.CorpusPrint("object of preposition");			
		}
		if (extractedDependency.matches("subj")) {
			//CentralCorpus.CorpusPrint("Subject");			
		}
		if (extractedDependency.matches("nsubj")) {
			//CentralCorpus.CorpusPrint("nominal subject.");
		}
		if (extractedDependency.matches("nsubjpass")) {
			//CentralCorpus.CorpusPrint("passive nominal subject");
		}
		if (extractedDependency.matches("csubj")) {
			//CentralCorpus.CorpusPrint("clausal subject");
		}
		if (extractedDependency.matches("csubjpass")) {
			//CentralCorpus.CorpusPrint("passivel clausal subject");	
		}
		if (extractedDependency.matches("cc")) {
			//CentralCorpus.CorpusPrint("Coordination.");		
		}
		if (extractedDependency.matches("conj")) {
			//CentralCorpus.CorpusPrint("conjunct");
		}
		if (extractedDependency.matches("expl")) {
			//CentralCorpus.CorpusPrint("expletive");
		}
		if (extractedDependency.matches("mod")) {
			//CentralCorpus.CorpusPrint("modifier.");	
		}
		if (extractedDependency.matches("amod")) {
			//CentralCorpus.CorpusPrint("adjectival modifier.");
			
		}
		if (extractedDependency.matches("appos")) {
			//CentralCorpus.CorpusPrint("appositional modifier");
			
		}
		if (extractedDependency.matches("adcvl")) {
			//CentralCorpus.CorpusPrint("adverbial clause modifier.");
			
		}
		if (extractedDependency.matches("det")) {
			//CentralCorpus.CorpusPrint("determiner");
			
		}
		if (extractedDependency.matches("predet")) {
			//CentralCorpus.CorpusPrint("Predeterminer");
			
		}
		if (extractedDependency.matches("preconj")) {
			//CentralCorpus.CorpusPrint("Preconjunct");
			
		}
		if (extractedDependency.matches("vmod")) {
			//CentralCorpus.CorpusPrint("reduced non-finite verbal modifier");
			
		}
		if (extractedDependency.matches("mwe")) {
			//CentralCorpus.CorpusPrint("Multiword expression modifier");
			
		}
		if (extractedDependency.matches("mark")) {
			//CentralCorpus.CorpusPrint("marker");
			
		}
		if (extractedDependency.matches("advmod")) {
			//CentralCorpus.CorpusPrint("adverbial modifier");
			
		}
		if (extractedDependency.matches("rcmod")) {
			//CentralCorpus.CorpusPrint("Relative clause modifier");
			
		}
		if (extractedDependency.matches("quantmod")) {
			//CentralCorpus.CorpusPrint("quantifier modifier");
			
		}
		if (extractedDependency.matches("nn")) {
			//CentralCorpus.CorpusPrint("noun compound modifier");
			
		}
		if (extractedDependency.matches("npadvmod")) {
			//CentralCorpus.CorpusPrint("noun phrase adverbial modiﬁer ");
			
		}
		if (extractedDependency.matches("tmod")) {
			//CentralCorpus.CorpusPrint("temporal modifier");
			
		}
		if (extractedDependency.matches("num")) {
			//CentralCorpus.CorpusPrint("numeric modifier");
			
		}
		if (extractedDependency.matches("number")) {
			//CentralCorpus.CorpusPrint("element of compound number");
			
		}
		if (extractedDependency.matches("prep")) {
			//CentralCorpus.CorpusPrint("prepositional modifier");
			
		}
		if (extractedDependency.matches("pos")) {
			//CentralCorpus.CorpusPrint("possession modifier");
			
		}
		if (extractedDependency.matches("prt")) {
			//CentralCorpus.CorpusPrint("phrasal verb participle");
			
		}
		if (extractedDependency.matches("parataxis")) {
			//CentralCorpus.CorpusPrint("parataxis");
			
		}
		if (extractedDependency.matches("punct")) {
			//CentralCorpus.CorpusPrint("punctuation");
			
		}
		if (extractedDependency.matches("ref")) {
			//CentralCorpus.CorpusPrint("rerferent");
			
		}
		if (extractedDependency.matches("sdep")) {
			//CentralCorpus.CorpusPrint("demantic dependent");
			
		}
		if (extractedDependency.matches("xsubj")) {
			//CentralCorpus.CorpusPrint("controlling subject");
			
		}
		//CentralCorpus.CorpusPrint("[END] Syntax.Analysis()");
	}
}
